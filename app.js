const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv/config');

app.use(bodyParser.json());

//routes imports
const taskRoute = require('./routes/tasks');

app.use('/tasks', taskRoute);

//db connection
mongoose.connect(
  process.env.DB_CONNECTION,
  {useNewUrlParser: true , useUnifiedTopology: true},
  () => console.log('connected to DB')
);

//LISTENER
app.listen(3000);
