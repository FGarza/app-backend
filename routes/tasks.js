const express = require('express');
const router = express.Router();
const Task = require('../models/Task');

//Get all tasks
router.get('/', async (req,res) => {
  try{
    console.log("got request");
    const tasks = await Task.find();
    res.json(tasks);
  }catch(err){
    res.json({message: err});
  }
});

//Get task details
router.get('/details/:taskId', async (req,res) => {
  try{
    const task = await Task.findById(req.params.taskId);
    res.json(task);
  }catch(err){
    res.json({message: err});
  }
});

//add task
router.post('/', async (req,res) => {
  const task = new Task({
    title: req.body.title,
    description: req.body.description
  });
  try{
    const savedTask = await task.save();
    res.json(savedTask);
  }catch(err){
    res.json({message: err});
  }
});

//Delete tasks
router.delete('/:taskId', async (req,res) => {
  try{
    const removedTask = await Task.remove({_id: req.params.taskId});
    res.json(removedTask);
  }catch(err){
    res.json({message: err});
  }


});

module.exports = router;
